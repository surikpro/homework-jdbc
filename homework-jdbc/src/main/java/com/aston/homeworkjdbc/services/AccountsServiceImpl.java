package com.aston.homeworkjdbc.services;

import com.aston.homeworkjdbc.domain.dto.AccountCreateDto;
import com.aston.homeworkjdbc.domain.dto.AccountDto;
import com.aston.homeworkjdbc.domain.dto.AccountUpdateDto;
import com.aston.homeworkjdbc.domain.dto.ProjectDto;
import com.aston.homeworkjdbc.repositories.AccountsRepository;
import com.aston.homeworkjdbc.repositories.ProjectsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AccountsServiceImpl implements AccountsService {

    private final AccountsRepository accountsRepository;
    private final ProjectsRepository projectsRepository;

    @Override
    public AccountDto getAccountInfo(Long accountId) {
        return accountsRepository.getAccountInfo(accountId);
    }

    @Override
    public AccountDto create(AccountCreateDto accountCreateDto) {
        return accountsRepository.create(accountCreateDto);
    }

    @Override
    public List<ProjectDto> getAllProjectsOfAccount(Long accountId) {
        return projectsRepository.getAllProjectsOfUser(accountId);
    }

    @Override
    public AccountDto update(Long accountId, AccountUpdateDto accountUpdateDto) {
        return accountsRepository.update(accountId, accountUpdateDto);
    }
}
