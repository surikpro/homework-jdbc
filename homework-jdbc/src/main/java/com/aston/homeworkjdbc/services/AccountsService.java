package com.aston.homeworkjdbc.services;

import com.aston.homeworkjdbc.domain.dto.AccountCreateDto;
import com.aston.homeworkjdbc.domain.dto.AccountDto;
import com.aston.homeworkjdbc.domain.dto.AccountUpdateDto;
import com.aston.homeworkjdbc.domain.dto.ProjectDto;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

public interface AccountsService {

    AccountDto getAccountInfo(@PathVariable Long accountId);
    AccountDto create(AccountCreateDto accountCreateDto);

    List<ProjectDto> getAllProjectsOfAccount(Long accountId);

    AccountDto update(Long accountId, AccountUpdateDto accountUpdateDto);
}
