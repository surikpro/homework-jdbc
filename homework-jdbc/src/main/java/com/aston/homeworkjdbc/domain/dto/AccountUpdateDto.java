package com.aston.homeworkjdbc.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

@Value
@Builder
@Jacksonized
@AllArgsConstructor
public class AccountUpdateDto {

    String firstName;
    String lastName;
}

