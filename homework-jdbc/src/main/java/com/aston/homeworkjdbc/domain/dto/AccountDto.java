package com.aston.homeworkjdbc.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AccountDto {
    private String firstName;
    private String lastName;
    private Set<ProjectDto> projects = new HashSet<>();
    private Set<PositionDto> positions = new HashSet<>();
    public void addProjectToAccount(ProjectDto projectDto) {
        this.projects.add(projectDto);
    }

    public void addPositionToAccount(PositionDto positionDto) {
        this.positions.add(positionDto);
    }

}
