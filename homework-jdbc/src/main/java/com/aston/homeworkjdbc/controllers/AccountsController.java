package com.aston.homeworkjdbc.controllers;

import com.aston.homeworkjdbc.domain.dto.AccountCreateDto;
import com.aston.homeworkjdbc.domain.dto.AccountDto;
import com.aston.homeworkjdbc.domain.dto.AccountUpdateDto;
import com.aston.homeworkjdbc.domain.dto.ProjectDto;
import com.aston.homeworkjdbc.services.AccountsService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class AccountsController {
    private final AccountsService accountsService;

    @GetMapping(path = "/accounts/{accountId}")
    public AccountDto getAccountInfo(@PathVariable Long accountId) {
        return accountsService.getAccountInfo(accountId);
    }

    @PostMapping("/accounts")
    public AccountDto createUser(@RequestBody AccountCreateDto accountCreateDto) {
        System.out.println("controller " + accountCreateDto);
        return accountsService.create(accountCreateDto);
    }

    @GetMapping(path = "/accounts/{accountId}/projects")
    public List<ProjectDto> getAllProjectsOfUser(@PathVariable Long accountId) {
        return accountsService.getAllProjectsOfAccount(accountId);
    }

    @PatchMapping("/accounts/{accountId}")
    public AccountDto update(@PathVariable(name = "accountId") Long accountId,
                          @RequestBody AccountUpdateDto accountUpdateDto) {
        return accountsService.update(accountId, accountUpdateDto);
    }




}
