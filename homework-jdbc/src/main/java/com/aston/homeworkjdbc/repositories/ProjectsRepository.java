package com.aston.homeworkjdbc.repositories;

import com.aston.homeworkjdbc.domain.dto.AccountUpdateDto;
import com.aston.homeworkjdbc.domain.dto.ProjectDto;
import com.aston.homeworkjdbc.domain.entity.Account;

import java.util.List;

public interface ProjectsRepository {
    List<ProjectDto> getAllProjectsOfUser(Long accountId);
}
