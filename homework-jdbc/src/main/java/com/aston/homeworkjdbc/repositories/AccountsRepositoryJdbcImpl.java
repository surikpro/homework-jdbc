package com.aston.homeworkjdbc.repositories;

import com.aston.homeworkjdbc.domain.dto.*;
import com.aston.homeworkjdbc.domain.entity.Account;
import com.aston.homeworkjdbc.domain.entity.Project;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;


@Repository
@RequiredArgsConstructor
public class AccountsRepositoryJdbcImpl implements AccountsRepository {
    private final DataSource dataSource;
    private Connection connection;

    //language=SQL
    private static final String SQL_SELECT_ACCOUNT_INFO_BY_ID = "SELECT acc.account_id, first_name, last_name,project_name, position_title FROM account AS acc\n" +
            "INNER JOIN accounts_projects ON acc.account_id = accounts_projects.account_id\n" +
            "INNER JOIN project ON project.project_id = accounts_projects.project_id\n" +
            "INNER JOIN position ON acc.account_id = position.account_id\n" +
            "WHERE acc.account_id = ?;";

    //language=SQL
    private static final String SQL_SELECT_ACCOUNT_BY_ID = "SELECT acc.account_id, first_name, last_name FROM account AS acc\n" +
            "WHERE acc.account_id = ?";
    //language=SQL
    private static final String SQL_INSERT_CREATE_ACCOUNT =
            "insert into account(first_name, last_name) values (?, ?)";
    //language=SQL
    private static final String SQL_INSERT_UPDATE_INTO_ACCOUNT =
            "UPDATE account SET first_name = ?, last_name = ? WHERE account.account_id = ?";




    @Override
    public AccountDto create(AccountCreateDto accountCreateDto) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_CREATE_ACCOUNT);
            preparedStatement.setString(1, accountCreateDto.getFirstName());
            preparedStatement.setString(2, accountCreateDto.getLastName());
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        System.out.println(accountCreateDto);
        return AccountDto.builder()
                .firstName(accountCreateDto.getFirstName())
                .lastName(accountCreateDto.getLastName())
                .projects(new HashSet<>())
                .build();
    }

//    @Override
//    public List<Project> getAllProjectsOfAccount(Long accountId) {
//        return null;
//    }

    @Override
    public AccountDto getAccountInfo(Long accountId) {
        AccountDto accountDto = new AccountDto();
        try {
            connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ACCOUNT_INFO_BY_ID);
            preparedStatement.setLong(1, accountId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                accountDto.setFirstName(resultSet.getString(2));
                accountDto.setLastName(resultSet.getString(3));
                accountDto.addProjectToAccount(ProjectDto.builder()
                                .projectName(resultSet.getString(4))
                        .build());
                accountDto.addPositionToAccount(PositionDto.builder()
                                .positionTitle(resultSet.getString(5))
                        .build());
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return AccountDto.builder()
                .firstName(accountDto.getFirstName())
                .lastName(accountDto.getLastName())
                .projects(accountDto.getProjects())
                .positions(accountDto.getPositions())
                .build();
    }

    @Override
    public AccountDto update(Long accountId, AccountUpdateDto accountUpdateDto) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_UPDATE_INTO_ACCOUNT);
            preparedStatement.setString(1, accountUpdateDto.getFirstName());
            preparedStatement.setString(2, accountUpdateDto.getLastName());
            preparedStatement.setLong(3, accountId);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return getAccountInfo(accountId);
    }

}
