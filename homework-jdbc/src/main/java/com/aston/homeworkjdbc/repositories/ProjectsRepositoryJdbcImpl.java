package com.aston.homeworkjdbc.repositories;

import com.aston.homeworkjdbc.domain.dto.ProjectDto;
import com.aston.homeworkjdbc.domain.entity.Project;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class ProjectsRepositoryJdbcImpl implements ProjectsRepository {

    private final DataSource dataSource;
    private Connection connection;

    private static final String SQL_SELECT_PROJECTS_BY_ACCOUNT_ID = "SELECT * FROM project " +
            "INNER JOIN accounts_projects ON project.project_id = accounts_projects.project_id " +
            "WHERE accounts_projects.account_id = ?";

    @Override
    public List<ProjectDto> getAllProjectsOfUser(Long accountId) {
        List<ProjectDto> userProjects = new ArrayList<>();
        try {
            connection = dataSource.getConnection();

            PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_PROJECTS_BY_ACCOUNT_ID);
            preparedStatement.setLong(1, accountId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                String projectName = resultSet.getString(2);
                userProjects.add(new ProjectDto(projectName));
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return userProjects;
    }
}
