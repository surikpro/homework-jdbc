package com.aston.homeworkjdbc.repositories;

import com.aston.homeworkjdbc.domain.dto.AccountCreateDto;
import com.aston.homeworkjdbc.domain.dto.AccountDto;
import com.aston.homeworkjdbc.domain.dto.AccountUpdateDto;
import com.aston.homeworkjdbc.domain.entity.Account;
import com.aston.homeworkjdbc.domain.entity.Project;

import java.util.List;

public interface AccountsRepository {
    AccountDto create(AccountCreateDto accountCreateDto);
//    List<Project> getAllProjectsOfAccount(Long accountId);
    AccountDto getAccountInfo(Long accountId);

    AccountDto update(Long accountId, AccountUpdateDto accountUpdateDto);
}
